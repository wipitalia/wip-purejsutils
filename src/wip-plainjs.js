var wipwrp = window.wipwrp || {};

wipwrp.plainjs = (function() {


    
	self.is_touch_device = function() {
		return 'ontouchstart' in window        // works on most browsers 
			|| navigator.maxTouchPoints;       // works on IE10/11 and Surface
	};


	self.notify = function(obj){

		/* Usage:

			TRADITIONALLY:

			wipwrp.plainjs.notify({
				where : '#myWrapper',
				smoothScroll : true,
				gapElementSelector : '#masthead',
				scrollSpeed : 0.8,
				kind : 'success',
				what : 'La tua richiesta è stata inviata correttamente!'
			});

			WITH SMOOTH SCROLLING:

			wipwrp.plainjs.notify({
				where : '#myWrapper',
				kind : 'success',
				what : 'La tua richiesta è stata inviata correttamente!'
			});
		*/

		// set Defaults
		var where = obj.where || "body"; 							// univoque tag, class or id
		var kind = obj.kind || "info"; 								// info, error, alert, success ...
		var what = obj.what || "no news, good news";
		
		var smoothScroll = obj.smoothScroll || false; 				// move scrolling to notification
		var gapElementSelector = obj.gapElementSelector || null; 	// element to move to
		var scrollSpeed = obj.scrollSpeed || 0.5; 					// speed of smoothscroll

		// insert notification
		var note = document.createElement("div");
		note.addClass('notification');
		note.addClass(kind);
		note.innerHTML = what;
		var f = document.querySelector(where);
		f.insertBefore(note, f.firstChild);

		// use hastag or avoid it scrolling to area:
		if(smoothScroll) {
			var smoothScrollOpt = {
				elementSelector : where,
				scrollSpeed : scrollSpeed
			}
			if(gapElementSelector!=null) {
				smoothScrollOpt.gapElementSelector = gapElementSelector;
			}
			wipwrp.plainjs.smoothScroll(smoothScrollOpt);
		} else {
			if (where.indexOf('#') !== -1) {
			location.href=window.location.pathname+window.location.search+where;
			}
		}

	}







	self.nospamEmail = function(){
		var el = document.querySelectorAll('.nospam-email');
		for (var i = 0; i < el.length; i++) {
			var emailStr = el[i].dataset.name + '@' + el[i].dataset.domain;
			el[i].innerHTML = '<a href="mailto:'+emailStr+'">'+emailStr+'</a>';
		};
	}

	/*
	* Styles
	*/

	self.windowWidth = function() {
		return window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
	}
	self.windowHeight = function() {
		return window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
	}

	self.windowScrollLeft = function() {
		return window.pageXOffset || document.documentElement.scrollLeft;
	}
	self.windowScrollTop = function() {
		return window.pageYOffset || document.documentElement.scrollTop;
	}

	/*
	* Ajax Methods
	*/

	self.serialize = function(form) {
	    var field, l, s = [];
	    if (typeof form == 'object' && form.nodeName == "FORM") {
	        var len = form.elements.length;
	        for (var i=0; i<len; i++) {
	            field = form.elements[i];
	            if (field.name && !field.disabled && field.type != 'file' && field.type != 'reset' && field.type != 'submit' && field.type != 'button') {
	                if (field.type == 'select-multiple') {
	                    l = form.elements[i].options.length; 
	                    for (var j=0; j<l; j++) {
	                        if(field.options[j].selected)
	                            s[s.length] = encodeURIComponent(field.name) + "=" + encodeURIComponent(field.options[j].value);
	                    }
	                } else if ((field.type != 'checkbox' && field.type != 'radio') || field.checked) {
	                    s[s.length] = encodeURIComponent(field.name) + "=" + encodeURIComponent(field.value);
	                }
	            }
	        }
	    }
	    return s.join('&').replace(/%20/g, '+');
	}


	self.getAjax = function(obj) {
		var xhr = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
		xhr.open('GET', obj.url);
		xhr.onreadystatechange = function() {
			if (xhr.readyState>3 && xhr.status===200) {
				obj.success(xhr.responseText);
			}
		};
		xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
		xhr.send();
		return xhr;
	}
	/*
	* example request
	getAjax({
		url: 'http://jsonplaceholder.typicode.com/posts/1',
			success: function(data) {
			console.log(data);
		}
	 });
	*/

	self.postAjax = function(obj) {
		var params = typeof obj.data === 'string' ? obj.data : Object.keys(obj.data).map(
			function(k){ return encodeURIComponent(k) + '=' + encodeURIComponent(obj.data[k]); }
			).join('&');

		var xhr = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
		xhr.open('POST', obj.url);
		xhr.onreadystatechange = function() {
			if (xhr.readyState>3 && xhr.status===200) { obj.success(xhr.responseText); }
		};
		xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
		xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		// http://stackoverflow.com/questions/7210507/ajax-post-error-refused-to-set-unsafe-header-connection
		//xhr.setRequestHeader('Content-length', params.length);
		xhr.send(params);
		return xhr;
	}
	/*
	* example request
	postAjax({
		url: 'http://foo.bar/',
		data: 'p1=1&p2=Hello+World',
		success: function(data) {
			console.log(data);
		}
	});
	* example request with data object
	postAjax({
		url: 'http://foo.bar/',
		data: {
			p1: 1,
			p2: 'Hello World'
		},
		success: function(data) {
			console.log(data);
		}
	});
*/
	self.smoothScroll = function(options){
		/* usage: 
			wipwrp.plainjs.smoothScroll({
				elementSelector : '#my-selector .example',
				gapElementSelector : '#my-fixed-header',
				scrollSpeed : 0.5
			});
		*/
		
		/* defaults management */
		var defaults = {
			elementSelector : 'body',
			gapElementSelector : false,
			scrollSpeed : 1
		};
		var opt = {};
	    opt.elementSelector = options.hasOwnProperty('elementSelector') ? options.elementSelector : defaults.elementSelector;
	    opt.gapElementSelector = options.hasOwnProperty('gapElementSelector') ? options.gapElementSelector : defaults.gapElementSelector;
	    opt.scrollSpeed = options.hasOwnProperty('scrollSpeed') ? options.scrollSpeed : defaults.scrollSpeed;

		/* manage Gap height value (fixed headers) */
		var gapElementH;
		if(opt.gapElementSelector!==defaults.gapElementSelector) {
			gapElementH = document.querySelector(opt.gapElementSelector).offsetHeight;
		} else {
			gapElementH = 0;
		}
		/* calculate target position */
		var choords = document.querySelector(opt.elementSelector).getBoundingClientRect();

		var scrollT = document.documentElement.scrollTop || document.body.scrollTop;
		var scrollL = document.documentElement.scrollLeft || document.body.scrollLeft;
		
		var position =  {
		  top: (choords.top + scrollT) - (gapElementH),
		  left: choords.left + scrollL
		}
		/* let's funk ... */
		if(TweenMax) {
			TweenMax.to(window, opt.scrollSpeed, {scrollTo:{y:position.top, x:position.left}});
		} else {
			window.scrollTo(0, position.top);
		}
	}

	

	self.disableScroll = function() {
		window.addEventListener('DOMMouseScroll', preventDefault);
		window.onwheel = preventDefault; // modern standard
		window.onmousewheel = document.onmousewheel = preventDefault; // older browsers, IE
		window.ontouchmove  = preventDefault; // mobile
		document.onkeydown  = preventDefaultForScrollKeys;
	}

	self.enableScroll = function() {
		window.removeEventListener('DOMMouseScroll', preventDefault);
		window.onmousewheel = document.onmousewheel = null;
		window.onwheel = null;
		window.ontouchmove = null;
		document.onkeydown = null;
	}

	self.documentReady = function(callback) {
		// in case the document is already rendered
		if (document.readyState!=='loading') {
			callback();
		} else {
			document.addEventListener('DOMContentLoaded', callback);
		}
	}

	self.createForm = function(obj) {
		var servURL = obj.servURL || ""; 
		var params = obj.params || {'':''}; // {'nome':'Mario','cognome':'Rossi','id':123}
		var method = obj.method || "post"; // post/get
		var form = document.createElement("form");
		form.setAttribute("method", method);
		form.setAttribute("action", servURL);
		for(var key in params) {
			var hiddenField = document.createElement("input");
			hiddenField.setAttribute("type", "hidden");
			hiddenField.setAttribute("name", key);
			hiddenField.setAttribute("value", params[key]);
			form.appendChild(hiddenField);
		}
		document.body.appendChild(form);
		form.submit();
	}

	return self;

})();




/**
* @method addEventListener
* @param {Object} obj The object to add an event listener to.
* @param {String} event The event to listen for.
* @param {Function} funct The function to execute when the event is triggered.
* @param {Boolean} evtCapturing True to do event capturing, false to do event bubbling.
*/
wipwrp.addEventListener = (function() {
	if (window.addEventListener) {
		return function(obj, event, funct, evtCapturing) {
			if (obj) {
				obj.addEventListener(event, funct, evtCapturing);
			};
		};
	} else if (window.attachEvent) {
		return function(obj, event, funct) {
			if (obj) {
				obj.attachEvent("on" + event, funct);
			}
		};
	}
})();

/**
* @method removeEventListener
* @param {Object} obj The object to remove an event listener from.
* @param {String} event The event that was listened for.
* @param {Function} funct The function that was executed when the event is triggered.
* @param {Boolean} evtCapturing True if event capturing was done, false otherwise.
*/

wipwrp.removeEventListener = (function() {
	if (window.removeEventListener) {
		return function(obj, event, funct, evtCapturing) {
			obj.removeEventListener(event, funct, evtCapturing);
		};
	} else if (window.detachEvent) {
		return function(obj, event, funct) {
			obj.detachEvent("on" + event, funct);
		};
	}
})();



/* ------------------------------------------ */
/* ----------------- DOM PROPERIES EXTENSIONS */
/* ------------------------------------------ */

// Remove element
Element.prototype.remove = function() {
	if (this.parentElement) {
		this.parentElement.removeChild(this);
	}
};
//ex: document.getElementById("my-element").remove();

NodeList.prototype.remove = HTMLCollection.prototype.remove = function() {
	for(var i = this.length - 1; i >= 0; i--) {
		if(this[i] && this[i].parentElement) {
			this[i].parentElement.removeChild(this[i]);
		}
	}
};
// ex: document.getElementsByClassName("my-elements").remove();

/*
* Elements' Attributes
*/

Element.prototype.hasClass =  function(className) {
	return this.classList ? this.classList.contains(className) : new RegExp('\\b'+ className+'\\b').test(this.className);
};

Element.prototype.addClass = function(className) {
	if (this.classList){
		this.classList.add(className);
	} else {
		this.className += ' ' + className;
	}
};
// ex: document.querySelector('body').addClass('loading');
NodeList.prototype.addClass = HTMLCollection.prototype.addClass = function (className) {
	for(var i = this.length - 1; i >= 0; i--) {
		if (this[i].classList){
			this[i].classList.add(className);
		} else {
			this[i].className += ' ' + className;
		}
	}
};
// ex: document.querySelectorAll('.className').addClass('loading');

Element.prototype.removeClass = function(className) {
	if (this.classList) {
		this.classList.remove(className);
	} else {
		this.className = this.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
	}
};
// ex: document.querySelector('body').removeClass('loading');
NodeList.prototype.removeClass = HTMLCollection.prototype.removeClass = function (className) {
	for(var i = this.length - 1; i >= 0; i--) {
		if (this[i].classList) {
			this[i].classList.remove(className);
		} else {
			this[i].className = this[i].className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
		}
	}
};
// ex: document.querySelectorAll('.className').removeClass('loading');

Element.prototype.toggleClass = function (className) {
	(this.hasClass(className)) ? this.removeClass(className) : this.addClass(className);
};
// ex: document.querySelector('body').toggleClass('loading');

NodeList.prototype.toggleClass = HTMLCollection.prototype.toggleClass = function (className) {
	for(var i = this.length - 1; i >= 0; i--) {
		(this[i].hasClass(className)) ? this[i].removeClass(className) : this[i].addClass(className);
	}
};
// ex: document.querySelectorAll('.className').toggleClass('loading');



// Utilities
Element.prototype.on = function(event, selector, callback) {
	this.addEventListener(event, function(e) {
		var qs = this.querySelectorAll(selector);
		if (qs) {
			var el = e.target || e.srcElement, index = Array.prototype.indexOf.call(qs, el);
			if (index > -1) {
				callback.call(el, e);
			}
		}
	});
};

// like a jquery "on" (to be continued) 
NodeList.prototype.onEvent = function(evt, action) {
	for (i = 0; i < this.length; i++) {
		this[i].addEventListener(evt, action);
	}
};
